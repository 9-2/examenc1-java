package com.example.examenc1_java;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {
    private TextView lblNombreBanco;
    private TextView lblNombreCliente;
    private TextView lblSaldo;
    private TextView lblMovimientos;
    private TextView lblCantidad;
    private EditText txtCantidad;
    private Button btnDeposito;
    private Button btnRetiro;
    private Button btnRegresar;
    private CuentaBanco cuenta = new CuentaBanco(0, "", "", 0);;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);



        btnDeposito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                depositar();
            }
        });

        btnRetiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retirar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }


    private void iniciarComponentes() {
        lblNombreBanco = (TextView) findViewById(R.id.lblNombreBanco);
        lblNombreCliente = (TextView) findViewById(R.id.lblNombreCliente);
        lblSaldo = (TextView) findViewById(R.id.lblSaldo);
        lblMovimientos = (TextView) findViewById(R.id.lblMovimientos);
        lblCantidad = (TextView) findViewById(R.id.lblCantidad);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        btnDeposito = (Button) findViewById(R.id.btnDeposito);
        btnRetiro = (Button) findViewById(R.id.btnRetiro);
        btnRegresar= (Button) findViewById(R.id.btnRegresar);
    }

    private void depositar() {
        if (txtCantidad.getText().toString().isEmpty()) {
            Toast.makeText(this, "Capture la cantidad", Toast.LENGTH_SHORT).show();
        } else {
            float cantidad = Float.parseFloat(txtCantidad.getText().toString()); // 8000
            float resultado = cuenta.depositar(cantidad); // 8000
            float saldoActualizado = Float.parseFloat(lblSaldo.getText().toString());
            float tot = resultado + saldoActualizado;
            lblSaldo.setText("" + tot);
        }


    }

    private void retirar() {
        if (txtCantidad.getText().toString().isEmpty()) {
            Toast.makeText(this, "Capture la cantidad", Toast.LENGTH_SHORT).show();
        } else {
            float cantidad = Float.parseFloat(txtCantidad.getText().toString());
            float saldoActualizado = Float.parseFloat(lblSaldo.getText().toString());
            float tot;

            tot = saldoActualizado - cantidad;
            lblSaldo.setText("" + tot);
        }

    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("BANCO NACIONAL SOMEX");
        confirmar.setMessage("Regresar a MainActivity");
        confirmar.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        // regresar
        confirmar.show();

    }



}
