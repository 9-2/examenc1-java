package com.example.examenc1_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    // EditText
    private EditText txtNumCuenta;
    private EditText txtNombre;
    private EditText txtBanco;
    private EditText txtSaldo;

    // Botones
    private Button btnEnviar;
    private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enviar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                salir();
            }
        });
    }

    private void iniciarComponentes() {
        txtNumCuenta = (EditText) findViewById(R.id.txtNumCuenta);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtBanco = (EditText) findViewById(R.id.txtBanco);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

    }

    private void enviar() {
        String numCuenta;
        String nombre;
        String banco;
        String saldo;

        banco = getApplicationContext().getResources().getString(R.string.strBanco);


        Bundle bundle = new Bundle();
        bundle.putString("nombre", txtNombre.getText().toString());
        bundle.putString("banco", txtBanco.getText().toString());
        bundle.putString("saldo", txtSaldo.getText().toString());

        Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);

    }

    private void salir() {

        finish();
    }

}