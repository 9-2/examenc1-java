package com.example.examenc1_java;

public class CuentaBanco {

    private int numCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    public CuentaBanco(int numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    // Set & Get
    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public int getNumCuenta() {
        return this.numCuenta;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return  this.nombre;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getBanco() {
        return  this.banco;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public float getSaldo() {
        return this.saldo;
    }

    public float depositar(float saldo) {
        float ingreso = 0;
        saldo += ingreso;

        return saldo;
    }

    public boolean retirar(float saldo) {
        float retiro = 0;
        boolean verificar;
        if(retiro < saldo) {
            verificar = false;
        }else {
            verificar = true;
        }

        return verificar;
    }


}
